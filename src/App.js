import React, {useEffect} from 'react';
import {connect} from "react-redux";
import {Route, Switch} from "react-router";
import {loadUserThunk} from "./Store/Auth/authActions";
import './index.css';

import Register from "./Components/Register/Register";
import Login from "./Components/Login/Login";
import TodoApp from "./Components/TodoApp/TodoApp";
import UnknownPage from "./Components/UnknownPage/UnknownPage";
import SeparateTask from "./Components/SeparateTask/SeparateTask";
import MyProfile from "./Components/MyProfile/MyProfile";
import Chat from "./Components/Chat/Chat";
import Room from "./Components/Chat/Room/Room";
import TicTac from "./Components/TicTac/TicTac";
import TicTacRoom from "./Components/TicTac/TicTacRoom/TicTacRoom";
import Navbar from "./Components/NewBar/NewBar";


function App({loadUserThunk, ...props}) {

    useEffect(() => {
        loadUserThunk();
        console.log('loaduser');
        //eslint-disable-next-line
    }, []);


    return (
        <div className="AppContainer">
            {props.auth.isAuthenticated && <Navbar/>}
            <Switch>
                <Route exact path='/' component={TodoApp}/>
                <Route exact path='/todo/:id' component={SeparateTask}/>
                <Route exact path='/profile' component={MyProfile}/>
                <Route exact path='/chat' component={Chat}/>
                <Route exact path='/chat/:id' component={Room}/>
                <Route exact path='/tictac' component={TicTac}/>
                <Route exact path='/tictac/:id' component={TicTacRoom}/>
                <Route exact path='/login' component={Login}/>
                <Route exact path='/register' component={Register}/>
                <Route component={UnknownPage}/>
            </Switch>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth
    }
};

export default connect(mapStateToProps, {loadUserThunk})(App);
