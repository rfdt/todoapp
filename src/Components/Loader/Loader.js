import React from 'react';
import styles from './Loader.module.css';

const Loader = (props) => {
    return (
        <div className={styles['lds-hourglass']}>
        </div>
    );
};

export default Loader;