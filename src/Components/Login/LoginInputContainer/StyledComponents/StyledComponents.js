import styled from "styled-components";

export const Field = styled.input`
  font-size: 1em;
  margin: 3%;
  padding: 10px;
  border: none;
  border-bottom: ${(props) => props.border || "1px solid #e0e0e0"};
  background-color: transparent;
  outline: none;
  transition: 0.3s;
  max-width: 100%;
  width: 70%;
  font-family: "Maiandra GD";

  :focus {
    border-bottom: ${(props) => props.border || "1px solid mediumorchid"};
  }
`;

export const RegisterError = styled.div`
    margin: 1%;
    font-size: 1.5em;
    color: rgba(255, 0, 0, 0.7);
    font-weight: bold;
    visibility: ${(props) => props.visibility || "hidden"};
    
    @media (max-width: 575px) {
        font-size: 1em;
        align: center;
    }
`;
