import React from 'react';
import styles from "../Login.module.css";
import * as Yup from "yup";
import {Formik} from "formik";
import {Field, RegisterError} from './StyledComponents/StyledComponents'
import {NavLink} from "react-router-dom";
import {connect} from "react-redux";
import {loginUserThunk} from "../../../Store/Auth/authActions";

const LoginInputContainer = (props) => {

    const validationSchema = Yup.object({
        email: Yup.string().required("Required").email('Email expected'),
        password: Yup.string().required("Required")
    });

    const initialValues = {email: "", password: ""};

    return (
        <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={(values, {setSubmitting}) => {
                props.loginUserThunk(values);
                setSubmitting(false);
            }}
        >
            {({
                  handleSubmit,
                  handleChange,
                  values,
                  errors,
                  touched,
                  handleBlur,
                  isSubmitting
              }) => (
                <div className={styles.logininputcontainer}>
                    {props.error.id !== null ? <RegisterError
                            visibility={(props.error.id === 'LOGIN_FAIL' && 'visible') || (props.error.id === 'AUTH_FAILED' && 'visible')}> {props.error.msg} </RegisterError> :
                        <RegisterError/>}
                    <form onSubmit={handleSubmit}>
                        <div className={styles.logininputs}>
                            <div className={styles.witherror}>
                                <div className={styles.name}>
                                    <div className="inputaroundtext">Email</div>
                                    <Field type="text" name="email" className={styles.logininput}
                                           placeholder="Enter your email" onChange={handleChange} value={values.email}
                                           onBlur={handleBlur}
                                           border={errors.email && touched.email && "1px solid red"}/>
                                </div>
                                {errors.email && touched.email && (
                                    <div className={styles.error}>{errors.email}</div>
                                )}
                            </div>
                            <div className={styles.witherror}>
                                <div className={styles.password}>
                                    <div className="inputaroundtext">Password</div>
                                    <Field type="password" name="password" value={values.password}
                                           className={styles.logininput}
                                           placeholder="Enter your Password" onChange={handleChange}
                                           onBlur={handleBlur}
                                           border={errors.password && touched.password && "1px solid red"}/>
                                </div>
                                {errors.password && touched.password && (
                                    <div className={styles.error}>{errors.password}</div>
                                )}
                            </div>
                            <input type='submit' className={styles.loginbutton} value='Login' disabled={isSubmitting}/>
                            <div className={styles.alredylogin}>Not registered? <NavLink className={styles.link}
                                                                                         to='/register'>Register</NavLink>
                            </div>
                        </div>
                    </form>
                </div>
            )}
        </Formik>
    );

};


let mapStateToProps = state => {
    return {
        error: state.error
    }
};

export default connect(mapStateToProps, {loginUserThunk})(LoginInputContainer);
