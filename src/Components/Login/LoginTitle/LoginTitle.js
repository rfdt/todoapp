import React from 'react';
import styles from "../Login.module.css";

const LoginTitle = (props) => {
    return (
        <div className={styles.logintitle}>
            Login Page
        </div>
    );
};

export default LoginTitle;