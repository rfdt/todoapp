import React from 'react';
import styles from './Login.module.css';
import AppTitle from "../Register/AppTitle/AppTitle";
import LoginTitle from "./LoginTitle/LoginTitle";
import LoginInputContainer from "./LoginInputContainer/LoginInputContainer";
import {Redirect} from "react-router";
import {connect} from "react-redux";

const Login = (props) => {

    if(props.auth.isAuthenticated === true) return <Redirect to='/' />;

    return (
            <div className={styles.logincontainer}>
                <AppTitle/>
                <LoginTitle/>
                <LoginInputContainer/>
            </div>
    );
};

let mapStateToProps = (state) => {
    return {
        auth: state.auth
    }
};

export default connect(mapStateToProps, null)(Login);