import React from 'react';
import TodoBlock from "../TodoBlock/TodoBlock";
import {Redirect} from "react-router";
import {connect} from "react-redux";



const TodoApp = (props) => {

    if(props.auth.isAuthenticated === false) return <Redirect to='/login' />;

    return (
            <TodoBlock/>
    );
};

let mapStateToProps = (state) => {
    return {
        auth: state.auth
    }
};

export default connect(mapStateToProps, null)(TodoApp);