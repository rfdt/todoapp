import axios from 'axios';

export const getSeparateTask = (id,token) =>{

    // Headers
    const config = {
        headers: {
            'Content-type': 'application/json'
        }
    };

    // If token, add to headers
    if (token) {
        config.headers['todo-auth-token'] = token;
    }

    return axios.get(`/todo/gettodo/${id}`, config)

};

export const addSeparateTask = (id,data,token) =>{

    // Headers
    const config = {
        headers: {
            'Content-type': 'application/json'
        }
    };

    // If token, add to headers
    if (token) {
        config.headers['todo-auth-token'] = token;
    }

    return axios.post(`/todo/addtask/${id}`, data, config);


};

export const toggleTaskCompleted = (idtodo, idtask, token) => {
    const config = {
        headers: {
            'Content-type': 'application/json'
        }
    };

    // If token, add to headers
    if (token) {
        config.headers['todo-auth-token'] = token;
    }

    return axios.get(`/todo/updatetaskstatus/${idtodo}/${idtask}`, config);
};

export const deleteTask = (idtodo, idtask, token) => {
    const config = {
        headers: {
            'Content-type': 'application/json'
        }
    };

    // If token, add to headers
    if (token) {
        config.headers['todo-auth-token'] = token;
    }

    return axios.get(`/todo/deletetask/${idtodo}/${idtask}`, config);
};

export const addTodoToProject = (idtodo, idproject, token) => {
    const config = {
        headers: {
            'Content-type': 'application/json'
        }
    };

    // If token, add to headers
    if (token) {
        config.headers['todo-auth-token'] = token;
    }

    return axios.get(`/todo/addtodotoproject/${idtodo}/${idproject}`, config);
};

export const deleteTodoFromProject = (idtodo, idproject, token) => {
    const config = {
        headers: {
            'Content-type': 'application/json'
        }
    };

    // If token, add to headers
    if (token) {
        config.headers['todo-auth-token'] = token;
    }

    return axios.get(`/todo/deletetodofromproject/${idtodo}/${idproject}`, config);
};






