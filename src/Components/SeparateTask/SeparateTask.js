import React, {useEffect, useState} from 'react';
import {Redirect, withRouter} from "react-router";
import style from './SeparateTask.module.css'
import {
    addSeparateTask,
    addTodoToProject,
    deleteTask, deleteTodoFromProject,
    getSeparateTask,
    toggleTaskCompleted
} from "./SeparateAPI";
import {connect} from "react-redux";
import {changeUserTodoCompletedThunk} from "../../Store/Todos/todosActions";


const SeparateTodo = (props) => {

    const completedHandler = () => {
        props.completedHandler(props.id);
    };

    const deleteHandler = () => {
        props.deleteTaskHandler(props.id);
    };

    return (
        <div className={style.todo}>
            <div className={props.completed ? style.todotextdone : style.todotext}>
                {props.data}
            </div>
            <div className={style.todobuttons}>
                <input type="button" className={style.tododonebutton} value="&#10004;" onClick={completedHandler}/>
                <input type="button" className={style.tododeletebutton} value="&#10007;"
                       onClick={deleteHandler}/>
            </div>
        </div>);
};

const SeparateTask = (props) => {

    const [projects, setProjects] = useState([]);
    const [addToProjectMode, setAddToProjectMode] = useState(false);
    const [projectAddToTodo, setProjectAddToTodo] = useState('');
    const [addMode, setAddMode] = useState(false);
    const [newTaskTitle, setnewTaskTitle] = useState('');
    const [currentTodo, setCurrentTodo] = useState({
        _id: '',
        completed: false,
        creator: props.auth.isLoaded && props.auth.isAuthenticated ? props.auth.user.id : '',
        project: "",
        data: "",
        date: "",
        tasks: [],
    });

    useEffect(() => {
        if (props.auth.isAuthenticated) {
            let cleanupFunction = false;
            getSeparateTask(props.match.params.id, props.token).then(res => {
                    if (!cleanupFunction) setCurrentTodo(res.data)
                }
            ).catch(err => console.log(err));

            return () => {
                cleanupFunction = true;
            }
        }
        //eslint-disable-next-line
    }, [props.auth.isAuthenticated]);

    useEffect(() => {
        setProjects(props.projects);
        setProjectAddToTodo(props.projects[0] ? props.projects[0]._id : '');
    }, [props.projects]);

    const toggleTaskCompletedHandler = (idtask) => {

        toggleTaskCompleted(props.match.params.id, idtask, props.token)
            .then(res => {
                let tasks = [...currentTodo.tasks];
                // eslint-disable-next-line array-callback-return
                tasks.map(task => {
                    if (task._id === idtask) task.completed = !task.completed
                });
                setCurrentTodo(prevState => ({
                    ...prevState,
                    tasks: tasks
                }));
            })
            .catch(err => console.log(err.response.data));
    };

    const deleteTaskHadler = (idtask) => {

        deleteTask(props.match.params.id, idtask, props.token).then(res => {
            let tasks = [...currentTodo.tasks];
            // eslint-disable-next-line array-callback-return
            const filteredTasks = tasks.filter(task => task._id !== idtask);

            setCurrentTodo(prevState => ({
                ...prevState,
                tasks: filteredTasks
            }));
        }).catch(error => {
            console.log({...error});
        });

    };

    const handler = (e) => {
        setnewTaskTitle(e.currentTarget.value);
    };

    const clickedHandler = () => {
        if (newTaskTitle.length > 0) {
            const newTask = {data: newTaskTitle};
            addSeparateTask(currentTodo._id, newTask, props.token).then(res => {
                    setCurrentTodo((prevState) => ({
                        ...prevState,
                        tasks: res.data.tasks
                    }));
                    setAddMode(false);
                }
            )
                .catch(error => {
                    console.log(error);
                    setAddMode(false);
                });
            setnewTaskTitle('');
        }

    };

    const addTodoToProjectHandler = () => {
        if (projectAddToTodo !== '') {
            addTodoToProject(currentTodo._id, projectAddToTodo, props.token)
                .then(res => {
                    setCurrentTodo(res.data)
                })
                .catch(err => console.log(err))
        }
    };

    const deleteTodoFromProjectHandler = () => {
        deleteTodoFromProject(currentTodo._id, projectAddToTodo, props.token).then(res => setCurrentTodo(res.data)).catch(err => console.log(err));
    };

    const keyDownHandler = (e) => {
        if (e.key === 'Enter' && newTaskTitle.length > 0) {
            const newTask = {data: newTaskTitle};
            addSeparateTask(currentTodo._id, newTask, props.token).then(res => {
                    setCurrentTodo((prevState) => ({
                        ...prevState,
                        tasks: res.data.tasks
                    }));
                    setAddMode(false);
                }
            )
                .catch(error => {
                    console.log(error);
                    setAddMode(false);
                });
            setnewTaskTitle('');
        }

    };

    const returnProjectName = () => {
        const project = projects.filter((project) => project._id === currentTodo.project);
        return project[0] ? project[0].name : '';
    };

    const availableProjectHandler = () => {
        if (addToProjectMode) {
            setAddToProjectMode(false);
        } else {
            setAddToProjectMode(true);
        }
    };

    const changeUserTodoCompletedNon = () => {
        props.changeUserTodoCompletedThunk(currentTodo._id);
        setCurrentTodo(prevState => ({
            ...prevState,
            completed: false
        }))
    };

    const changeUserTodoCompletedTrue = () => {
        props.changeUserTodoCompletedThunk(currentTodo._id);
        setCurrentTodo(prevState => ({
            ...prevState,
            completed: true
        }))
    };


    if (props.auth.isAuthenticated === false && props.auth.isLoaded === true) return <Redirect to='/'/>;

    if (props.auth.isLoaded === true) {
        if (props.auth.user.id !== currentTodo.creator && props.auth.isLoaded === true) return <Redirect to='/'/>
    }

    return (
        <>
            {currentTodo ?
                <div className={style.todoseparetecontainer}>
                    <div className={style.todosepareteblockname}>
                        {/* eslint-disable */}
                        Todo: <b>{currentTodo.data}</b>{currentTodo.completed ?
                        <div className={style.changeUserTodoCompletedNon}
                             onClick={changeUserTodoCompletedNon}>&#9989;</div> :
                        <div className={style.changeUserTodoCompletedTrue}
                             onClick={changeUserTodoCompletedTrue}>&#10060;</div>}
                        {/* eslint-enable */}
                    </div>
                    <div className={style.todosepareteblockname}>
                        Date: <b>{new Date(currentTodo.date).toLocaleDateString()}</b>
                    </div>
                    {currentTodo.project === '' ?
                        addToProjectMode ?
                            <div className={style.todoprojectblockmain}>
                                <div className={style.todoprojectblockheader}>
                                    <div style={{cursor: "pointer"}} onClick={availableProjectHandler}>
                                        Add to project:&nbsp;
                                    </div>
                                    <select value={projectAddToTodo} onChange={(event) => {
                                        setProjectAddToTodo(event.target.value);
                                    }} className={style.projectSelect}>
                                        {projects.length !== 0
                                            ?
                                            projects.map((projectRender) => (
                                                <option key={projectRender._id}
                                                        value={projectRender._id}>{projectRender.name}</option>))
                                            : <option value={''}>No project</option>}
                                    </select>
                                </div>
                                <input type='submit' className={style.addbuttonproject} value='&#10004;'
                                       onClick={addTodoToProjectHandler}/>
                            </div>
                            : <div onClick={availableProjectHandler} className={style.availableProjectHandler}>Add to
                                project</div>
                        : <div className={style.todoprojectblockbottom}>In project&nbsp;<b>{returnProjectName()}</b>
                            {/* eslint-disable-next-line */}
                            <div className={style.deleteprojectbutton}
                                 onClick={deleteTodoFromProjectHandler}>&#10060;</div>
                        </div>
                    }
                    <div className={style.todoseparatemainblock}>
                        {currentTodo.tasks.length !== 0 ?
                            <div className={style.tasklist}>Todo Tasks: <b>{currentTodo.tasks.length}</b></div> : null}
                        {currentTodo.tasks.length === 0 ?
                            <div className={style.tasklist}><b>Non tasks</b></div> : currentTodo.tasks.map(task => (
                                <SeparateTodo completed={task.completed} id={task._id} data={task.data}
                                              key={task._id} completedHandler={toggleTaskCompletedHandler}
                                              deleteTaskHandler={deleteTaskHadler}/>))
                        }
                        {addMode ?
                            <div className={style.todoblockinput}>
                                <input type="text" value={newTaskTitle} className={style.todoinput}
                                       placeholder="Your new task" onChange={handler} onKeyDown={keyDownHandler}/>
                                <input type='submit' className={style.addbutton} value='&#10148;'
                                       onClick={clickedHandler}/>
                            </div>
                            :
                            <div className={style.addnewtasktext} onClick={() => {
                                setAddMode(true)
                            }}>Click to add new Task</div>
                        }
                    </div>
                </div>
                :
                null
            }
        </>
    );
};

const mapStateToProps = (state) => {
    return {
        token: state.auth.token,
        auth: state.auth,
        projects: state.auth.isAuthenticated ? state.auth.user.projects : []
    }
};

export default connect(mapStateToProps, {changeUserTodoCompletedThunk})(withRouter(SeparateTask));
