import React from 'react';
import styles from './UnknownPage.module.css'
import {NavLink} from "react-router-dom";
import {connect} from "react-redux";
import {Redirect} from "react-router";

const UnknownPage = (props) => {

    if(props.auth.isAuthenticated === false) return <Redirect to='/login' />;

    return (
        <div className={styles.unknownpage}>
            Unknown Page
            <NavLink className={styles.link} to={'/'}>To HomePage</NavLink>
        </div>
    );
};

let mapStateToProps = (state) => {
    return {
        auth: state.auth
    }
};

export default connect(mapStateToProps, null)(UnknownPage);