import React from 'react';
import styles from "../Register.module.css";

const RegisterTitle = (props) => {
    return (
        <div className={styles.registertitle}>
            Register your account
        </div>
    );
};

export default RegisterTitle;