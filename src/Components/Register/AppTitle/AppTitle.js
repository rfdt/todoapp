import React from 'react';
import styles from "../Register.module.css";

const AppTitle = (props) => {
    return (
        <div className={styles.apptitle}>TodoApp</div>
    );
};

export default AppTitle;