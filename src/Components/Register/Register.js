import React from 'react';
import styles from './Register.module.css';
import AppTitle from "./AppTitle/AppTitle";
import RegisterTitle from "./RegisterTitle/RegisterTitle";
import RegisterInputContainer from "./RegisterInputContainer/RegisterInputContainer";
import {Redirect} from "react-router";
import {connect} from "react-redux";

const Register = (props) => {

    if(props.auth.isAuthenticated === true) return <Redirect to='/' />;

    return (
        <div className={styles.registercontainer}>
            <AppTitle/>
            <RegisterTitle/>
            <RegisterInputContainer/>
        </div>
    );
};

let mapStateToProps = (state) => {
    return {
        auth: state.auth
    }
};

export default connect(mapStateToProps, null)(Register);