import React from 'react';
import styles from "../Register.module.css";
import {Formik} from "formik";
import * as Yup from "yup";
import {Field, RegisterError} from "./StyledComponents/StyledComponents";
import {NavLink} from "react-router-dom";
import {connect} from "react-redux";
import {registerUserThunk} from "../../../Store/Auth/authActions";


const RegisterInputContainer = (props) => {

    const validationSchema = Yup.object({
        login: Yup.string().required("Required").matches("^[a-zA-Z][a-zA-Z0-9-_\\.]{1,20}$", 'Minimum 2 characters. First character letter'),
        email: Yup.string().required("Required").email("Email only"),
        password: Yup.string().required("Required").matches(
        "^.*(?=.{8,})(?=.*[a-zA-Z])(?=.*\\d)(?=.*[!#$%&? \"]).*$",
        "Password must contain 8 characters: one number, one letter and one character such as !#$%&?"
        ),
        confirmPassword: Yup.string().required("Required").oneOf([Yup.ref("password"), null], "Passwords not confirmed")
    });

    const initialValues = {login: "", email: "", password: "", confirmPassword: ""};

    return (
        <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={(values, {setSubmitting}) => {

                const newUser = {
                  name: values.login,
                  email: values.email,
                  password: values.password
                };
                props.registerUserThunk(newUser);
                setSubmitting(false);
            }}
        >
            {({
                  handleSubmit,
                  handleChange,
                  values,
                  errors,
                  touched,
                  handleBlur,
                  isSubmitting
              }) => (
                <div className={styles.registerinputcontainer}>
                    {props.error.id !== null ? <RegisterError visibility = {props.error.id === 'REGISTER_FAIL' && 'visible'}> {props.error.msg} </RegisterError> : <RegisterError/>}
                    <form onSubmit={handleSubmit}>
                        <div className={styles.registerinputs}>
                            <div className={styles.witherror}>
                                <div className={styles.name}>
                                    <div className="inputaroundtext">Login</div>
                                    <Field type="text" placeholder="Enter your login" name="login" onChange={handleChange}
                                           value={values.login} onBlur={handleBlur} border={errors.login && touched.login && "1px solid red"}/>
                                </div>
                                {errors.login && touched.login && (
                                    <div className={styles.error}>{errors.login}</div>
                                )}
                            </div>
                            <div className={styles.witherror}>
                                <div className={styles.email}>
                                    <div className="inputaroundtext">Email</div>
                                    <Field type="text" name="email" onChange={handleChange} value={values.email}
                                           onBlur={handleBlur} placeholder="Enter your email" border={errors.email && touched.email && "1px solid red"}/>
                                </div>
                                {errors.email && touched.email && (
                                    <div className={styles.error}>{errors.email}</div>
                                )}
                            </div>
                            <div className={styles.witherror}>
                                <div className={styles.password}>
                                    <div className="inputaroundtext">Password</div>
                                    <Field type="password" name="password" onChange={handleChange} value={values.password}
                                           onBlur={handleBlur} placeholder="Enter your password" border={errors.password && touched.password && "1px solid red"}/>
                                </div>
                                {errors.password && touched.password && (
                                    <div className={styles.error}>{errors.password}</div>
                                )}
                            </div>
                            <div className={styles.witherror}>
                                <div className={styles.password}>
                                    <div className={styles.inputaroundtext}>Confirm</div>
                                    <Field type="password" name="confirmPassword" onChange={handleChange}
                                           value={values.confirmPassword} onBlur={handleBlur}
                                           placeholder="Confirm password" border={errors.confirmPassword && touched.confirmPassword && "1px solid red"}/>
                                </div>
                                {errors.confirmPassword && touched.confirmPassword && (
                                    <div className={styles.error}>{errors.confirmPassword}</div>
                                )}
                            </div>
                            <input type='submit' className={styles.registerbutton} disabled={isSubmitting} value='Register'/>
                            <div className={styles.alredyregister}>Already registered? <NavLink className={styles.link}
                                                                                          to='/login'>Login</NavLink></div>
                        </div>
                    </form>
                </div>
            )}
        </Formik>
    );
};

let mapStateToProps = state => {
  return {
      error: state.error
  }
};

export default connect(mapStateToProps, {registerUserThunk})(RegisterInputContainer);