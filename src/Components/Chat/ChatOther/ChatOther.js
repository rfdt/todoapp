import React from 'react';
import style from '../Chat.module.css';
import {NavLink} from "react-router-dom";


const ChatOther = (props) => {
    return (
        <div className={style.chat}>
            <div className={style.chattext}>
                <NavLink className={style.link} to={`/chat/${props.id}`}>{props.name}</NavLink>
            </div>
        </div>
    );
};

export default ChatOther;