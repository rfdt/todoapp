import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import {createChat, loadChats} from "../../Store/Chat/chatActions";
import Loader from "../Loader/Loader";
import {Redirect} from "react-router";
import style from './Chat.module.css';
import ChatOther from "./ChatOther/ChatOther";

const Chat = (props) => {

    const [addMode, setAddMode] = useState(false);
    const [newChatTitle, setNewChatTitle] = useState('');

    const keyDownHandler = (e) => {
        if (e.key === 'Enter' && newChatTitle.length > 0) {
            props.createChat(newChatTitle);
            setNewChatTitle('');
            setAddMode(false);
        }
    };

    const clickedHandler = () => {
        if (newChatTitle.length > 0) {
            props.createChat(newChatTitle);
            setNewChatTitle('');
            setAddMode(false);
        }
    };

    const handler = (e) => {
        setNewChatTitle(e.currentTarget.value);
    };

    useEffect(() => {
        props.loadChats();
        //eslint-disable-next-line
    }, []);

    if (props.auth.isAuthenticated === false && props.auth.isLoaded === true) return <Redirect to='/'/>;

    return (
        <>
            <div className={style.chatblockcontainer}>
                <div className={style.chatblockname}>
                    Welcome, RFdt
                </div>
                <div className={style.chattextheader}>
                    Chats
                </div>
                <div className={style.chatmainblock}>
                    {props.chat.isLoading ? <Loader/> :
                        props.chat.chats ? props.chat.chats.map(chat => <ChatOther key={chat._id} name={chat.name} id={chat._id}/>) :
                            <div>Mo chat now</div>
                    }
                    {addMode ?
                        <div className={style.chatblockinput}>
                            <input type="text" value={newChatTitle} className={style.chatinput}
                                   placeholder="New chat" onChange={handler} onKeyDown={keyDownHandler}/>
                            <input type='submit' className={style.addbutton} value='&#10148;'
                                   onClick={clickedHandler}/>
                        </div>
                        :
                        <div className={style.addnewchattext} onClick={() => {
                            setAddMode(true)
                        }}>Click to add new Chat</div>
                    }
                </div>
            </div>
        </>
    );
};

const mapStateToProps = (state) => {
    return ({
        chat: state.chat,
        auth: state.auth,
    })
};


export default connect(mapStateToProps, {loadChats, createChat})(Chat);