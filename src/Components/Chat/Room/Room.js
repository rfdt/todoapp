import React, {useEffect, useRef, useState} from 'react';
import {connect} from "react-redux";
import {Redirect, withRouter} from "react-router";
import socket from "../../../Core/socket";
import style from './Room.module.css';
import Message from "./Message/Message";
import MyMessage from "./MyMessage/MyMessage";

const Room = (props) => {

    const [currentRoomName, setCurrentRoomName] = useState('');
    const [users, setUsers] = useState([]);
    const [onlineUsers, setOnlineUsers] = useState([]);
    const [messages, setMessages] = useState([]);
    const [newMessageText, setNewMessageText] = useState('');

    const messageRef = useRef(null);

    useEffect(() => {
        if (props.auth.isAuthenticated) {
            const settings = {
                roomId: props.match.params.id,
                userId: props.auth.user.id,
                userName: props.auth.user.name
            };
            socket.emit('ROOM:JOIN', settings);

            socket.on('ROOM:USERJOINED', userJoinedFunc);

            socket.on('ROOM:DISCONNECTED', userDisconnected);

            socket.on('ROOM:MESSAGEFROMSERVER', newMassage);

            return () => {

                socket.disconnect();

                socket.removeListener('ROOM:USERJOINED', userJoinedFunc);

                socket.off('ROOM:DISCONNECTED', userDisconnected);

                socket.off('ROOM:MESSAGEFROMSERVER', newMassage);

            }
        }
        //eslint-disable-next-line
    }, [props.auth.isAuthenticated]);

    useEffect(()=>{
        const onlineusers = [];
        users.forEach((user)=>{
            if (onlineusers.indexOf(user.name) === -1){
                onlineusers.push(user.name);
            }
            setOnlineUsers(onlineusers);
        })
    }, [users]);

    useEffect(()=>{
        messageRef.current.scrollTo(0, 99999);
    }, [messages]);


    const userJoinedFunc = (payload) => {
        if (payload) {
            if (payload.users.length !== users.length) {
                setUsers(payload.users);
            }
            if (payload.messages.length !== messages.length) {
                setMessages(payload.messages)
            }
            if (payload.name !== currentRoomName) {
                setCurrentRoomName(payload.name);
            }
        }
    };

    const userDisconnected = (payload) => {
        setUsers(payload);
    };

    const newMassage = (payload) => {
        setMessages(prevState => {
            return (
                [...prevState, payload]
            );
        });
    };

    const keyDownHandler = (e) => {
        if (e.key === 'Enter' && newMessageText.length > 0) {
            const newMessage = {
                roomId: props.match.params.id,
                text: newMessageText,
                creatorName: props.auth.user.name,
                creatorId: props.auth.user.id,
            };
            socket.emit('ROOM:MESSAGE', newMessage);
            setNewMessageText('');
        }
    };

    const clickedHandler = () => {
        if (newMessageText.length > 0) {
            const newMessage = {
                roomId: props.match.params.id,
                text: newMessageText,
                creatorName: props.auth.user.name,
                creatorId: props.auth.user.id,
            };
            socket.emit('ROOM:MESSAGE', newMessage);
            setNewMessageText('');
        }
    };

    if (props.auth.isAuthenticated === false && props.auth.isLoaded === true) return <Redirect to='/'/>;

    return (
        <>
            <div className={style.chatblockcontainer}>
                <div className={style.chatblockname}>
                    Welcome, {props.auth.user ? props.auth.user.name : ''}
                </div>
                <div className={style.chattextheader}>
                    Chat: {currentRoomName}
                </div>
                <div className={style.chattextheader}>
                    Online: {onlineUsers.length}
                </div>
                <div className={style.chatmainblock} ref={messageRef}>
                    {messages.length ? messages.map(message => (
                        message.creatorId === props.auth.user.id ?
                            <MyMessage creator={'You'} text={message.text} key={message._id}/> :
                            <Message creator={message.creatorName} text={message.text} key={message._id}/>
                    )) : <div>No messages</div>
                    }
                </div>
                <div className={style.todoblockinput}>
                    <input type="text" value={newMessageText} className={style.todoinput}
                           placeholder="Message" onChange={(e) => {
                        setNewMessageText(e.target.value)
                    }} onKeyDown={keyDownHandler}/>
                    <input type='submit' className={style.addbutton} value='&#10148;'
                           onClick={clickedHandler}/>
                </div>
            </div>
        </>
    );
};


const mapStateToProps = (state) => {
    return ({
        auth: state.auth,
    })
};

export default connect(mapStateToProps, null)(withRouter(Room));