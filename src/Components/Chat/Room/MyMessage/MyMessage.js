import React from 'react';
import style from '../Room.module.css';

const MyMessage = (props) => {
    return (
        <div className={style.mymessage}>
            <div className={style.messagecreator}>
                {props.creator}
            </div>
            <div className={style.messagetext}>
                {props.text}
            </div>
        </div>
    );
};

export default MyMessage;