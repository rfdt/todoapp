import React from 'react';
import style from '../Room.module.css';

const Message = (props) => {
    return (
        <div className={style.message}>
            <div className={style.messagecreator}>
                {props.creator}
            </div>
            <div className={style.messagetext}>
                {props.text}
            </div>
        </div>
    );
};

export default Message;