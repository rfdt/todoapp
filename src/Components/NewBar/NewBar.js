import React from 'react';
import styled from 'styled-components';
import Burger from './Burger/Burger';

const Nav = styled.nav`
  width: 100%;
  height: 55px;
  border-bottom: 2px solid #f1f1f1;
  padding: 0 20px;
  display: flex;
  justify-content: space-between;
  
  font-family: "Maiandra GD", serif;
  font-weight: bold;
  font-size: 1.5em;
  background-color: rgba(186, 85, 211, 0.5);
  border: none;
  margin-bottom: 10px;
  border-radius: 10px;

  .logo {
    margin-top: 5px;
    font-family: Logo, cursive;
    opacity: 0.8;
  }
  

`;

const Navbar = () => {
    return (
        <Nav>
            <div className='logo'>
                TodoApp
            </div>
            <Burger/>
        </Nav>
    )
};

export default Navbar
