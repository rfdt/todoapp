import React from 'react';
import styled from 'styled-components';
import styles from "../NewBar.module.css";
import {NavLink} from "react-router-dom";
import {logout} from "../../../Store/Auth/authActions";
import {connect} from "react-redux";

const Ul = styled.ul`
  list-style: none;
  display: flex;
  flex-flow: row nowrap;
  margin: 0;

  li {
    padding: 18px 10px;
  }

  @media (max-width: 768px) {
    flex-flow: column nowrap;
    align-items: center;
    background-color: rgba(186,85,211,1);
    position: fixed;
    transform: ${({open}) => open ? 'translateX(0)' : 'translateX(100%)'};
    top: 0;
    right: 0;
    height: 100vh;
    width: 300px;
    padding-top: 3.5rem;
    transition: transform 0.3s ease-in-out;
    margin: 0;
    z-index: 19;

    li {
      color: #fff;
    }
  }
`;




const RightNav = ({open, ...props}) => {

    const setOpenRec = () => {
        props.setOpen(!open);
    };

    return (
            <Ul open={open}>
                <li onClick={setOpenRec}><NavLink to='/' activeClassName={styles.active}
                                                  className={styles.links}>{'ToDo'}</NavLink></li>
                <li onClick={setOpenRec}><NavLink to='/chat' activeClassName={styles.active}
                                                  className={styles.links}>Chat</NavLink></li>
                <li onClick={setOpenRec}><NavLink to='/tictac' activeClassName={styles.active}
                                                  className={styles.links}>TicTac</NavLink></li>
                <li onClick={setOpenRec}><NavLink to='/profile' activeClassName={styles.active}
                                                  className={styles.links}>Profile</NavLink>
                </li>
                <li onClick={setOpenRec}><NavLink className={styles.links} onClick={() => {
                    props.logout()
                }} to="/login">Logout</NavLink></li>
            </Ul>
    )
};

export default connect(null, {logout})(RightNav);
