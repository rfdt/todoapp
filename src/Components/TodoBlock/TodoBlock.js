import React, {useEffect, useState} from 'react';
import styles from './TodoBlock.module.css'
import {connect} from "react-redux";
import TodoNameBlock from "./TodoNameBlock/TodoNameBlock";
import TodoInputBlock from "./TodoInputBlock/TodoInputBlock";
import TodoButtonTypeBlock from "./TodoButtonTypeBlock/TodoButtonTypeBlock";
import TodoTypeBlock from "./TodoTypeBlock/TodoTypeBlock";
import TodoListBlock from "./TodoListBlock/TodoListBlock";
import {loadUserTodosThunk} from "../../Store/Todos/todosActions";
import TodoProjectBlock from "./TodoProjectBlock/TodoProjectBlock";


const TodoBlock = ({loadUserTodosThunk, ...props}) => {

    const [tab, setTab] = useState('All');
    const [project, setProject] = useState('');

    useEffect(() => {
        loadUserTodosThunk();
        //eslint-disable-next-line
    }, []);

    return (
        <div className={styles.todoupblockcontainer}>
            < TodoNameBlock/>
            < TodoInputBlock/>
            < TodoButtonTypeBlock tab={tab} setTab={setTab}/>
            < TodoProjectBlock project={project} setProject={setProject}/>
            < TodoTypeBlock tab={tab}/>
            < TodoListBlock tab={tab} project={project}/>
        </div>);
};


export default connect(null, {loadUserTodosThunk})(TodoBlock);