import React, {useEffect, useState} from 'react';
import styles from '../TodoBlock.module.css';
import {connect} from "react-redux";
import {addNewUserProject} from "../../../Store/Todos/todosActions";

const TodoProjectBlock = ({project, setProject, projects, addNewUserProject}) => {

    const [isAvailableNewProject, setAvailableNewProject] = useState(false);
    const [newProjectTitle, setNewProjectTitle] = useState('');
    const [currentProjects, setCurrentProjects] = useState([{
        _id: '',
        name: 'All'
    }]);

    useEffect(() => {
        setCurrentProjects([
            {
                _id: '',
                name: 'All'
            },
            ...projects
        ]);
    }, [projects]);

    const handler = (e) => {
        setNewProjectTitle(e.currentTarget.value);
    };

    const clickedHandler = () => {
        const checkIndex = currentProjects.map(e => e.name).indexOf(newProjectTitle);
        if (newProjectTitle.length > 20) {
            setNewProjectTitle('Max length 20 symbols');
        } else {
            if (checkIndex === -1) {
                addNewUserProject(newProjectTitle.trim());
                setNewProjectTitle('');
                setAvailableNewProject(false);
            } else {
                setNewProjectTitle('Project Already Exist');
            }
        }
    };

    const availableNewProjectHandler = () => {
        if (isAvailableNewProject) {
            setAvailableNewProject(false);
            setNewProjectTitle('');
        } else {
            setAvailableNewProject(true);
            setNewProjectTitle('');
        }
    };

    return (
        <div className={styles.projectSelectWrapper}>
            <div className={styles.todoprojectblockheader}>
                <div onClick={availableNewProjectHandler}>
                    Projects:&nbsp;
                </div>
                <select value={project} onChange={(event) => {
                    setProject(event.target.value);
                }} className={styles.projectSelect}>
                    {currentProjects.length !== 0
                        ?
                        currentProjects.map((projectRender) => (
                            <option key={projectRender._id} value={projectRender._id}>{projectRender.name}</option>))
                        : <option value={'All'}>All</option>}
                </select>
            </div>
            {
                isAvailableNewProject ?
                    <div className={styles.todoblockinputproject}>
                        <input type="text" value={newProjectTitle} className={styles.todoinputproject}
                               placeholder="Your new project" onChange={handler}/>
                        <input type='submit' className={styles.addbuttonproject} value='&#10004;'
                               onClick={clickedHandler}/>
                    </div>
                    : null
            }
        </div>
    );
};

const mapStateToProps = state => ({
    projects: state.auth.user.projects
});

export default connect(mapStateToProps, {addNewUserProject})(TodoProjectBlock);