import React from 'react';
import styles from '../TodoBlock.module.css'
import {connect} from "react-redux";

const TodoNameBlock = ({name}) => {
    return (
        <div className={styles.todoblockname}>Welcome <b>{name}</b></div>
    );
};

let mapStateToProps = state => {
    return {
        name: state.auth.user.name
    }
};

export default connect(mapStateToProps,null)(TodoNameBlock);