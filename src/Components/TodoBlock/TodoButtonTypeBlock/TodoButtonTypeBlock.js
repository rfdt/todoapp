import React from 'react';
import styles from '../TodoBlock.module.css';

const TodoButtonTypeBlock = ({tab, setTab}) => {
    return (
        <div className={styles.todoblockbuttontype}>
            <input type="button" className={tab === 'All' ? styles.activetab : styles.nonactivetab} value="All"
                   onClick={() => {
                       setTab('All')
                   }}/>
            <input type="button" className={tab === 'Completed' ? styles.activetab : styles.nonactivetab}
                   value="Completed" onClick={() => {
                setTab('Completed')
            }}/>
            <input type="button" className={tab === 'Uncompleted' ? styles.activetab : styles.nonactivetab}
                   value="Uncompleted" onClick={() => {
                setTab('Uncompleted')
            }}/>
        </div>
    );
};

export default TodoButtonTypeBlock;