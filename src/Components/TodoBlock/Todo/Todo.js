import React from 'react';
import styles from "../TodoBlock.module.css";
import {connect} from "react-redux";
import {changeUserTodoCompletedThunk, deleteUserTodoThunk} from "../../../Store/Todos/todosActions";
import {NavLink} from "react-router-dom";

const Todo = ({title, completed, changeUserTodoCompletedThunk, deleteUserTodoThunk, id, tasks, projectName}) => {

    const todoDoneHandler = () => {
        changeUserTodoCompletedThunk(id);
    };

    const deleteTodoHandler = () => {
        deleteUserTodoThunk(id);
    };

    return (
        <div className={styles.todo}>
            <div className={styles.textblock}>
                <NavLink className={completed ? styles.todotextdone : styles.todotext} to={`/todo/${id}`}>
                    {title}
                </NavLink>
                {tasks.length !== 0 ? <div className={styles.task}>
                    &nbsp;&#10004;Tasks: {tasks.length}&#10004;
                </div> : null}
                {projectName !== '' ? <div className={styles.project}>{projectName}</div>: <div className={styles.noproject}></div>}
            </div>




            <div className={styles.todobuttons}>
                <input type="button" className={styles.tododonebutton} onClick={todoDoneHandler} value="&#10004;"/>
                <input type="button" className={styles.tododeletebutton} onClick={deleteTodoHandler} value="&#10007;"/>
            </div>
        </div>
    );
};

export default connect(null, {changeUserTodoCompletedThunk, deleteUserTodoThunk})(Todo);