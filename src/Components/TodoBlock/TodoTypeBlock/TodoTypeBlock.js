import React from 'react';
import styles from "../TodoBlock.module.css";

const TodoTypeBlock = ({tab}) => {
    return (
        <div className={styles.todotypetext}>Current tab: <b>{tab}</b></div>
    );
};

export default TodoTypeBlock;