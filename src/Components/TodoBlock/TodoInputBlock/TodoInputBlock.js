import React, {useState} from 'react';
import styles from "../TodoBlock.module.css";
import {connect} from "react-redux";
import {addUserTodoThunk} from "../../../Store/Todos/todosActions";


const TodoInputBlock = (props) => {

    const [newTodoTitle, setNewTodoTitle] = useState('');

    const handler = (e) => {
        setNewTodoTitle(e.currentTarget.value);
    };

    const clickedHandler = () => {
        if (newTodoTitle.length > 0) {
            props.addUserTodoThunk(newTodoTitle);
            setNewTodoTitle('')
        }
    };

    const keyDownHandler = (e) => {
        if(e.key === 'Enter' && newTodoTitle.length > 0) {
            props.addUserTodoThunk(newTodoTitle);
            setNewTodoTitle('');
        }
    };


    return (
        <div className={styles.todoblockinput}>
            <input type="text" value={newTodoTitle} className={styles.todoinput} onChange={handler} onKeyDown={keyDownHandler} placeholder="Your New ToDo"/>
            <input type='submit' className={styles.addbutton} onClick={clickedHandler} value='&#10148;'/>
        </div>
    );
};

export default connect(null, {addUserTodoThunk})(TodoInputBlock);