import React, {useEffect, useState} from 'react';
import styles from "../TodoBlock.module.css";
import Todo from "../Todo/Todo";
import Loader from "../../Loader/Loader";
import {connect} from "react-redux";

const TodoListBlock = (props) => {

    const [filteredTodos, setFilteredTodos] = useState([]);

    useEffect(() => {
        let filtered;
        switch (props.tab) {
            case 'All':
                filtered = props.todos;
                break;
            case  'Completed':
                filtered = props.todos.filter(todo => todo.completed === true);
                break;
            case 'Uncompleted':
                filtered = props.todos.filter(todo => todo.completed === false);
                break;
            default:
                setFilteredTodos(props.todos);
                break;
        }

        if (props.project !== '') {
            filtered = filtered.filter(todo => todo.project === props.project);
            setFilteredTodos(filtered);
        } else {
            setFilteredTodos(filtered);
        }
    }, [props.todos, props.tab, props.project]);

    const returnProjectName = (todoproject) => {
        const project = props.projects.filter((project) => project._id === todoproject);
        return project[0] ? project[0].name : '';
    };

    return (
        <div className={styles.todomainblock}>
            {props.loading ? <Loader/> :
                filteredTodos.length === 0 ? < div className={styles.todotypetext}>No todos</div> :

                    filteredTodos.map(todo => (
                        <Todo title={todo.data} completed={todo.completed} key={todo._id} id={todo._id}
                              tasks={todo.tasks} projectName={todo.project ? returnProjectName(todo.project) : ''} />
                    ))}
        </div>
    );

};

const mapStateToProps = (state) => ({
    todos: state.todos.todos,
    loading: state.todos.loading,
    projects: state.auth.isAuthenticated ? state.auth.user.projects : []
});

export default connect(mapStateToProps, null)(TodoListBlock);