import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import {Redirect} from "react-router";
import styles from './MyProfile.module.css';
import {logout} from "../../Store/Auth/authActions";

const MyProfile = (props) => {

    const [profile, setProfile] = useState({
        name: props.auth.isLoaded && props.auth.isAuthenticated ? props.auth.user.name : '',
        email: props.auth.isLoaded && props.auth.isAuthenticated ? props.auth.user.email : '',
        register_date: props.auth.isLoaded && props.auth.isAuthenticated ? props.auth.user.register_date : '',
    });

    useEffect(()=>{
        if (props.auth.user !== null) {
            setProfile(props.auth.user);
        }
        //eslint-disable-next-line
    }, [props.auth.isLoaded]);

    console.log(profile);

    if (props.auth.isAuthenticated === false && props.auth.isLoaded === true) return <Redirect to='/'/>;

    return (
        <>
            <div className={styles.todoupblockcontainer}>
                <div className={styles.profilePhoto} onClick={props.logout}>
                    {profile.name[0]? profile.name[0].toUpperCase() : ''}
                </div>
                <div className={styles.profileName}>
                    Name:&nbsp;<b>{profile.name}</b>
                </div>
                <div className={styles.profileEmail}>
                    Email:&nbsp;<b>{profile.email}</b>
                </div>
                <div className={styles.profileRegisterdate}>
                    Register Date:&nbsp;<b>{new Date(profile.register_date).toLocaleDateString()}</b>
                </div>
            </div>
        </>
    );
};

let mapStateToProps = (state) => {
    return {
        auth: state.auth
    }
};

export default connect(mapStateToProps, {logout})(MyProfile);