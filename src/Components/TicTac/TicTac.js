import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import {Redirect} from "react-router";
import style from './TicTac.module.css';

import Loader from "../Loader/Loader";
import TicTacOther from "./TicTacOther/TicTacOther";
import {createTictac, loadTictacs} from "../../Store/TicTac/tictacActions";


const TicTac = (props) => {

    const [addMode, setAddMode] = useState(false);
    const [newRoomTitle, setNewRoomTitle] = useState('');

    const keyDownHandler = (e) => {
        if (e.key === 'Enter' && newRoomTitle.length > 0) {
            props.createTictac(newRoomTitle);
            setNewRoomTitle('');
            setAddMode(false);
        }
    };

    const clickedHandler = () => {
        if (newRoomTitle.length > 0) {
            props.createTictac(newRoomTitle);
            setNewRoomTitle('');
            setAddMode(false);
        }
    };

    const handler = (e) => {
        setNewRoomTitle(e.currentTarget.value);
    };

    useEffect(() => {
        props.loadTictacs();
        //eslint-disable-next-line
    }, []);

    if (props.auth.isAuthenticated === false && props.auth.isLoaded === true) return <Redirect to='/'/>;

    return (
        <>
            <div className={style.chatblockcontainer}>
                <div className={style.chatblockname}>
                    Welcome {props.auth.isAuthenticated ? props.auth.user.name : ''}
                </div>
                <div className={style.chattextheader}>
                    Game Rooms
                </div>
                <div className={style.chatmainblock}>
                    {props.tictac.isLoading ? <Loader/> :
                        props.tictac.tictacs ? props.tictac.tictacs.filter(tictac => tictac.status === "Not Started").map(chat =>
                                <TicTacOther key={chat._id} name={chat.name} id={chat._id}/>) :
                            <div>Mo chat now</div>
                    }
                    {addMode ?
                        <div className={style.chatblockinput}>
                            <input type="text" value={newRoomTitle} className={style.chatinput}
                                   placeholder="New TicTac game" onChange={handler} onKeyDown={keyDownHandler}/>
                            <input type='submit' className={style.addbutton} value='&#10148;'
                                   onClick={clickedHandler}/>
                        </div>
                        :
                        <div className={style.addnewchattext} onClick={() => {
                            setAddMode(true)
                        }}>Click to add new Game</div>
                    }
                </div>
            </div>
        </>
    );
};
//TODO Сделать здесь потом фильтрацию, чтобы показывались только не начатые игры и не завершенные.
const mapStateToProps = (state) => {
    return ({
        tictac: state.tictac,
        auth: state.auth,
    })
};


export default connect(mapStateToProps, {loadTictacs, createTictac})(TicTac);