import React from 'react';
import style from '../TicTac.module.css';
import {NavLink} from "react-router-dom";


const TicTacOther = (props) => {
    return (
        <div className={style.chat}>
            <div className={style.chattext}>
                <NavLink className={style.link} to={`/tictac/${props.id}`}>{props.name}</NavLink>
            </div>
        </div>
    );
};

export default TicTacOther;