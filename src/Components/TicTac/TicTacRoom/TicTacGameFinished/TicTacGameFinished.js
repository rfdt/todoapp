import React from 'react';
import style from '../TicTacRoom.module.css'
import {NavLink} from "react-router-dom";

const TicTacGameFinished = (props) => {
    console.log(props.room.winner);
    return (
        <div className={style.tictackGame__finished}>
            Game Finished
            {props.room.winner === 'draw' ? null : props.room.winner === 'Opponent Surender' ?
                <div className={style.tictackGame__finished_winner}>Opponent Surrender</div> :
                <div className={style.tictackGame__finished_winner}>
                    Winner: {props.room.winner}
                </div>}
            {props.room.winner === 'draw' ? <div className={style.tictackGame__finished_winner}>
                Dead Heat
            </div> : null}
            <div>
                <NavLink to='/'>Return to main page</NavLink>
            </div>
        </div>
    );
};

export default TicTacGameFinished;