import React, {useEffect, useState} from 'react';
import style from '../TicTacRoom.module.css'
import socket from '../../../../Core/socket'

const ButtonBlock = (props) => {
    return (
        <>
            {!props.room.players.playerX.playerName &&
            <input type="button" onClick={props.pickX} className={style.tictackGame__pickplayer__button}
                   value="Player X"/>}
            {!props.room.players.playerO.playerName &&
            <input type="button" onClick={props.pickO} className={style.tictackGame__pickplayer__button}
                   value="Player O"/>}
        </>
    )
};

const TicTacGameNotStarted = (props) => {

    const [userName, setUserName] = useState('');

    useEffect(() => {
        if (props.auth.isAuthenticated) {
            setUserName(props.auth.user.name)
        }
        //eslint-disable-next-line
    }, [props.auth.isAuthenticated]);

    const clusterSettings = {
        roomId: props.room._id,
        playerName: props.auth.user ? props.auth.user.name : '',
        playerId: props.auth.user ? props.auth.user.id : '',
        players: props.room.players
    };

    const pickX = () => {
        socket.emit('TICTAC:PICKX', clusterSettings);
        props.setYourValue('X')
    };

    const pickO = () => {
        socket.emit('TICTAC:PICKO', clusterSettings);
        props.setYourValue('O')
    };

    return (
        <div className={style.tictackGame__pickplayer}>
            <div className={style.tictackGame__pickplayer__text}>Choose your figure</div>
            {props.room.players.playerX.playerName === userName || props.room.players.playerO.playerName === userName ? null :
                <ButtonBlock room={props.room} auth={props.auth} pickX={pickX} pickO={pickO}/>
            }
        </div>
    );
};

export default TicTacGameNotStarted;