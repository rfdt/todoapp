import React, {useRef} from 'react';
import style from '../../TicTacRoom.module.css';


const Values = (props) =>{
    return (
        <div className={props.value === 'X' ? style.tictackGame__fieldRow__cell__valueX : style.tictackGame__fieldRow__cell__valueO}>{props.value}</div>
    )
};

const TicTacCell = (props) => {

    const ref = useRef(null);

    const click = () =>{
        if(props.room.currentTurn === props.yourValue && !ref.current.innerHTML){
            props.move(props._id);
        }
    };

    return (
        <div className={style.tictackGame__fieldRow__cell} ref={ref} onClick={click}>
            {props.value ? <Values value={props.value}/> : null}
        </div>
    );
};

export default TicTacCell;