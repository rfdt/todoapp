import React from 'react';
import style from '../TicTacRoom.module.css';
import TicTacRow from "./TicTacRow/TicTacRow";

const TicTacInGame = (props) => {


    return (
        <>
            <div className={style.tictackGame__playerX}>Player X: <u>{props.room.players.playerX.playerName}</u></div>
            <div className={style.tictackGame__playerO}>Player O: <u>{props.room.players.playerO.playerName}</u></div>
            <div className={style.tictackGame__currentTurn}>
                Current Turn: <b><u>{props.room.currentTurn}</u></b>
            </div>
            <div className={style.tictackGame__field}>
                {props.cells.map((item,index)=>(
                    <TicTacRow cells={item} yourValue={props.yourValue} key={index} room={props.room} move={props.move}/>
                ))}
            </div>
        </>
    );
};

export default TicTacInGame;