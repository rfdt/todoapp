import React from 'react';
import style from '../../TicTacRoom.module.css';
import TicTacCell from "../TicTacCell/TicTacCell";

const TicTacRow = (props) => {
    return (
        <div className={style.tictackGame__fieldRow}>
            {props.cells.map((cell) => (
                <TicTacCell value={cell.value} yourValue={props.yourValue} key={cell._id} room={props.room}
                            _id={cell._id} move={props.move}/>
            ))}
        </div>
    );
};

export default TicTacRow;