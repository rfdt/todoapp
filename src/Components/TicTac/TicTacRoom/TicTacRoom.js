import React, {useEffect, useState} from 'react';
import style from './TicTacRoom.module.css';
import TicTacGameNotStarted from "./TicTacNotStarted/TicTacGameNotStarted";
import TicTacInGame from "./TicTacInGame/TicTacInGame";
import TicTacGameFinished from "./TicTacGameFinished/TicTacGameFinished";
import toMatrix from "../../../Utils/toMatrix";
import socket from "../../../Core/socket";
import {connect} from "react-redux";
import {withRouter} from "react-router";

const TicTacRoom = (props) => {

    const [room, setRoom] = useState({
        name: '',
        status: 'Not Started',
        players: {
            playerX: {
                playerName: ''
            },
            playerO: {
                playerName: ''
            }
        },
        currentTurn: '',
        cells: [],
        winner: ''
    });
    const [cells, setCells] = useState([]);
    const [yourValue, setYourValue] = useState('');

    useEffect(() => {
        const newCells = toMatrix(room.cells, 3);
        setCells(newCells);
    }, [room.cells]);

    useEffect(() => {
        if (props.auth.isAuthenticated) {
            const settings = {
                roomId: props.match.params.id,
            };

            socket.emit('TICTAC:JOIN', settings);

            socket.on('TICTAC:USERJOINED', tictacUserJoined);

            socket.on('TICTAC:PICKPLAYER', tictacPickPlayer);

            socket.on('TICTAC:MOVE', tictacNewMove);

            socket.on('TICTAC:DISCONNECTED', tictacDisconnect);


            return () => {
                socket.disconnect();

                socket.off('TICTAC:USERJOINED', tictacUserJoined);

                socket.off('TICTAC:PICKPLAYER', tictacPickPlayer);

                socket.off('TICTAC:MOVE', tictacNewMove);

                socket.off('TICTAC:DISCONNECTED', tictacDisconnect);
            }
        }
        //eslint-disable-next-line
    }, [props.auth.isAuthenticated]);

    const tictacUserJoined = (payload) => {
        setRoom(payload);
    };

    const tictacDisconnect = (payload) =>{
        console.log(payload);
        setRoom({
            ...payload,
            winner: 'Opponent Surender',
            status: 'Game Finished'
        })
    };

    const tictacPickPlayer = (payload) => {
        setRoom(payload);
    };

    const tictacNewMove = (payload) => {
        setRoom(payload);
    };

    const tictacMove = (cellId) =>{
        const tictacMoveData = {
            room: room,
            cellId: cellId,
            value: yourValue
        };
        socket.emit('TICTAC:MOVE', tictacMoveData);
    };

    return (
        <>
            <div className={style.tictackGame__container}>
                <div className={style.tictackGame__roomname}>
                    Room: <b>{room.name}</b>
                </div>
                <div className={style.tictackGame__status}>
                    Status: <b>{room.status}</b>
                </div>
                <hr/>
                {room.status === 'Not Started' &&
                <TicTacGameNotStarted room={room} auth={props.auth} setYourValue={setYourValue}/>}
                {room.status === 'In-Game' &&
                <TicTacInGame room={room} cells={cells} yourValue={yourValue} move={tictacMove}/>}
                {room.status === 'Game Finished' && <TicTacGameFinished room={room}/>}
            </div>
        </>
    );
};

const mapStateToProps = (state) => {
    return ({
        auth: state.auth,
    })
};

export default connect(mapStateToProps, null)(withRouter(TicTacRoom));