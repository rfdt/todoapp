import React from 'react';
import {logout} from "../../Store/Auth/authActions";
import {connect} from "react-redux";

import styles from "./Header.module.css";
import {NavLink} from "react-router-dom";

const Header = (props) => {
    return (
        <div className={styles.header}>
            <div className={styles.headertodologo}>TodoApp</div>
            <div className={styles.links}>
                <NavLink to='/' activeClassName={styles.active}>{'ToDo'}</NavLink>
                <NavLink to='/chat' activeClassName={styles.active}>Chat</NavLink>
                <NavLink to='/tictac' activeClassName={styles.active}>TicTac</NavLink>
                <NavLink to='/profile' activeClassName={styles.active}>Profile</NavLink>
                <NavLink onClick={() => {
                    props.logout()
                }} to="/login">Logout</NavLink>
            </div>
        </div>
    );
};

export default connect(null, {logout})(Header);