export default function toMatrix(arr, width) {
    return arr.reduce(function (rows, key, index) {
        index % width === 0
            ? rows.push([key])
            : rows[rows.length - 1].push(key);
        return rows;
    }, []);
}