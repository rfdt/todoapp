import {applyMiddleware, combineReducers, createStore} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";

import thunk from "redux-thunk";

import todosReducer from "./Todos/todosReducer";
import authReducer from "./Auth/authReducer";
import errorReducer from "./Error/errorReducer";
import chatReducer from "./Chat/chatReducer";
import tictacReducer from "./TicTac/tictacReducer";



const reducers = combineReducers({
    todos: todosReducer,
    auth: authReducer,
    chat: chatReducer,
    tictac: tictacReducer,
    error: errorReducer
});

let store = createStore(
    reducers,
    composeWithDevTools(
        applyMiddleware(thunk)
    )
);

export default store;