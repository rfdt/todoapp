import {
    AUTH_ERROR,
    LOGIN_FAIL,
    LOGIN_SUCCESS,
    LOGOUT_SUCCESS, REGISTER_FAIL,
    REGISTER_SUCCESS,
    USER_LOADED,
    USER_LOADING
} from "./authTypes";
import {ADD_NEW_USER_PROJECT} from "../Todos/todosTypes";

const initialState = {
    token: localStorage.getItem('todotoken'),
    isAuthenticated: false,
    isLoading: false,
    isLoaded: false,
    user: null
};

const authReducer = (state = initialState, action) => {
    let copyState;
    switch (action.type) {
        case USER_LOADING:
            return {
                ...state,
                isLoading: true,
                isLoaded: false
            };
        case USER_LOADED:
            return {
                ...state,
                user: {...action.data},
                isAuthenticated: true,
                isLoading: false,
                isLoaded: true
            };
        case LOGIN_SUCCESS:
        case REGISTER_SUCCESS:
            localStorage.setItem('todotoken', action.data.token);
            return {
                ...state,
                ...action.data,
                isAuthenticated: true,
                isLoading: false,
                isLoaded: true
            };
        case AUTH_ERROR:
        case LOGIN_FAIL:
        case LOGOUT_SUCCESS:
        case REGISTER_FAIL:
            localStorage.removeItem('todotoken');
            return {
                ...state,
                token: null,
                user: null,
                isAuthenticated: false,
                isLoading: false,
                isLoaded: true
            };
        case ADD_NEW_USER_PROJECT:
            copyState = {...state};
            copyState.user.projects = action.data;
            return copyState;
        default:
            return state;
    }
};

export default authReducer;
