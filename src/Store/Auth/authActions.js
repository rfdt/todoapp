import axios from 'axios';
import {
    AUTH_ERROR,
    LOGIN_FAIL,
    LOGIN_SUCCESS, LOGOUT_SUCCESS,
    REGISTER_FAIL,
    REGISTER_SUCCESS,
    USER_LOADED,
    USER_LOADING
} from "./authTypes";
import {clearErrors, returnErrors} from "../Error/errorActions";


export const tokenConfig = (getState) => {
    // Get token from localstorage
    const token = getState().auth.token;

    // Headers
    const config = {
        headers: {
            'Content-type': 'application/json'
        }
    };

    // If token, add to headers
    if (token) {
        config.headers['todo-auth-token'] = token;
    }

    return config;
};

export const loadUserThunk = () => (dispatch, getState) => {
    // User loading
    dispatch(userLoadingAC());
    dispatch(clearErrors());

    if(!getState().auth.token) {
        dispatch(authErrorAC());
        return 0;
    }

    axios
        .get('/auth/user', tokenConfig(getState))
        .then(res =>
            dispatch(userLoadedAC(res.data))
        )
        .catch(err => {
            console.log(err);
            dispatch(returnErrors(err.response.data.msg, err.response.status, 'AUTH_FAILED'));
            dispatch(authErrorAC());
        });
};

export const loginUserThunk = ({email, password}) => (dispatch) => {

    // Headers
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };

    // Request body
    const body = JSON.stringify({email, password});

    dispatch(clearErrors());

    axios
        .post('/auth/login', body, config)
        .then(res =>
            dispatch(logincSuccesAc({...res.data}))
        )
        .catch(err => {
            console.log(err.data);
            dispatch(returnErrors(err.response.data.msg, err.response.status, 'LOGIN_FAIL'));
            dispatch(loginFailAC());
        });
};

export const registerUserThunk = ({name, email, password}) => (dispatch) => {

    dispatch(clearErrors());
    // Headers
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };

    // Request body
    const body = JSON.stringify({name, email, password});

    axios
        .post('/auth/register', body, config)
        .then(res =>
            dispatch(registerSuccesAc(res.data))
        )
        .catch(err => {
            // console.log(err.response.data.msg);
            dispatch(returnErrors(err.response.data.msg, err.response.status, 'REGISTER_FAIL'));
            dispatch(registerFailAC());
        });
};

export const logout = () => ({type: LOGOUT_SUCCESS });

const registerFailAC = () => ({type: REGISTER_FAIL});
const loginFailAC = () => ({type: LOGIN_FAIL});

const registerSuccesAc = (data) => ({
    type: REGISTER_SUCCESS,
    data: data
});

const logincSuccesAc = (data) => ({
    type: LOGIN_SUCCESS,
    data: data
});


const userLoadingAC = () => ({
    type: USER_LOADING
});

const userLoadedAC = (data) =>({
    type: USER_LOADED,
    data: data
});

const authErrorAC = () => ({type: AUTH_ERROR });
