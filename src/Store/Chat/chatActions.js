import axios from 'axios';
import {ADD_CHAT, LOADED_CHATS, START_LOADING_CHATS} from "./chatTypes";

const tokenConfig = (getState) => {
    // Get token from localstorage
    const token = getState().auth.token;

    // Headers
    const config = {
        headers: {
            'Content-type': 'application/json'
        }
    };

    // If token, add to headers
    if (token) {
        config.headers['todo-auth-token'] = token;
    }

    return config;
};

export const loadChats = () => {
    return (dispatch, getState)=>{
        dispatch(startLoadingChatsAC());
        axios.get('/chat', tokenConfig(getState)).then(res=>{
            dispatch(loadedChats(res.data));
        }).catch(error=>{console.log(error)});
    }
};

export const createChat = (name) =>{
    return (dispatch, getState)=>{
        axios.post('/chat/createchat', {name}, tokenConfig(getState)).then(res=>{
            dispatch(addChatAC(res.data));
        }).catch(error =>{
            console.log(error);
        })
    }
};

export const startLoadingChatsAC = () =>({
    type: START_LOADING_CHATS
});

export const loadedChats = (payload) =>({
    type: LOADED_CHATS, payload: payload
});

export const addChatAC = (payload) => ({
    type: ADD_CHAT, payload: payload
});
