import {ADD_CHAT, LOADED_CHATS, START_LOADING_CHATS} from "./chatTypes";


const initialState = {
    chats: null,
    isLoading: false
};

const chatReducer = (state = initialState, action) =>{
    let copyState;
    switch (action.type) {
        case START_LOADING_CHATS:
            copyState = {...state};
            copyState.isLoading = true;
            return copyState;
        case LOADED_CHATS:
            copyState = {...state};
            copyState.isLoading = false;
            copyState.chats = [...action.payload];
            return copyState;
        case ADD_CHAT :
            copyState = {...state};
            copyState.chats = [...copyState.chats, action.payload];
            return copyState;
        default:
            return state;
    }
};

export default chatReducer;