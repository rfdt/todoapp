import {
    ADD_NEW_TODO,
    CHANGE_TODO_COMPLETED, DELETE_TODO,
    FINISH_LOAD_USER_TODOS,
    LOAD_USER_TODOS,
    START_LOAD_USER_TODOS
} from "./todosTypes";
import {LOGOUT_SUCCESS} from "../Auth/authTypes";

const initialState = {
    loading:false,
    todos: []
};

const todosReducer = (state = initialState, action) => {
    let copyState;
    switch (action.type) {
        case ADD_NEW_TODO:
            copyState={...state};
            copyState.todos = [...state.todos, action.data];
            return copyState;
        case DELETE_TODO:
            copyState = {...state};
            copyState.todos = [...state.todos].filter((todo) => todo._id !== action.id);
            return copyState;
        case START_LOAD_USER_TODOS:
            copyState = {...state};
            copyState.loading = true;
            return copyState;
        case FINISH_LOAD_USER_TODOS:
            copyState = {...state};
            copyState.loading = false;
            return copyState;
        case LOAD_USER_TODOS:
            copyState = {...state};
            copyState.todos = [...action.data];
            return copyState;
        case CHANGE_TODO_COMPLETED:
            copyState = {...state};
            copyState.todos = [...state.todos];
            copyState.todos.forEach((todo)=>{
                if (todo._id === action.id)  todo.completed = !todo.completed;
            });
            return copyState;
        case LOGOUT_SUCCESS:
            copyState = {...state};
            copyState.todos = [];
            return copyState;
        default:
            return state;
    }
};



export default todosReducer;