import {
    ADD_NEW_TODO, ADD_NEW_USER_PROJECT,
    CHANGE_TODO_COMPLETED, DELETE_TODO,
    FINISH_LOAD_USER_TODOS,
    LOAD_USER_TODOS,
    START_LOAD_USER_TODOS
} from "./todosTypes";
import axios from 'axios';


const tokenConfig = (getState) => {
    // Get token from localstorage
    const token = getState().auth.token;

    // Headers
    const config = {
        headers: {
            'Content-type': 'application/json'
        }
    };

    // If token, add to headers
    if (token) {
        config.headers['todo-auth-token'] = token;
    }

    return config;
};

export const loadUserTodosThunk = () => (
    (dispatch, getState) =>{
        dispatch(startLoadUserTodosAC());
        axios.get('/todo/getusertodo', tokenConfig(getState)
        )
            .then(res => {
                    const data = res.data;
                    dispatch(loadUserTodosAC(data));
                    dispatch(finishLoadUserTodosAC());
                }
            )
            .catch(error=>{
                console.log("loadUserTodosThunk error", error);
            })
    }
);

export const addUserTodoThunk = (title) => (
    (dispatch, getState) => {
        axios.post('/todo/createtodo', {data: title}, tokenConfig(getState))
            .then(res => {
                const data = res.data;
                dispatch(addTodoAC(data));
            })
            .catch(error=>{
                console.log('addUserTodoThunk', error);
            })
    }
);

export const changeUserTodoCompletedThunk = (id) =>(
    (dispatch, getState) => {
        axios.get(`/todo/updatetodostatus/${id}`, tokenConfig(getState))
            .then(res=>{
                dispatch(changeUserTodoCompletedAC(res.data.id));
            })
            .catch(error=>{
                console.log('changeUserTodoCompletedThunk', error);
            })
    }
);



export const deleteUserTodoThunk = (id) =>(
    (dispatch, getState) => {
        axios.get(`/todo/deletetodo/${id}`, tokenConfig(getState))
            .then(res=>{
                dispatch(deleteUserTodoCompletedAC(id));
            })
            .catch(error=>{
                console.log('changeUserTodoCompletedThunk', error);
            })
    }
);

export const addNewUserProject = (name) =>(
    (dispatch, getState) => {
        axios.post(`/todo/addprojects`, {name: name},  tokenConfig(getState))
            .then(res=>{
                dispatch(addNewUserProjectAC(res.data.projects));
            })
            .catch(error=>{
                console.log('changeUserTodoCompletedThunk', error);
            })
    }
);


export const addTodoAC = (data) => ({type: ADD_NEW_TODO, data: data});

export const deleteUserTodoCompletedAC = (id) => ({type: DELETE_TODO, id: id});

export const changeUserTodoCompletedAC = (id) => ({type: CHANGE_TODO_COMPLETED, id: id});

export const loadUserTodosAC = (data) => ({type: LOAD_USER_TODOS, data: data});

export const startLoadUserTodosAC = () => ({type: START_LOAD_USER_TODOS});

export const finishLoadUserTodosAC = () => ({type: FINISH_LOAD_USER_TODOS});

export const addNewUserProjectAC = (data) => ({
    type: ADD_NEW_USER_PROJECT, data: data
});
