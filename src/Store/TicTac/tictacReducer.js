import {ADD_TICTAC, LOADED_TICTACS, START_LOADING_TICTAC} from "./tictacTypes";


const initialState = {
    tictacs: null,
    isLoading: false
};

const chatReducer = (state = initialState, action) =>{
    let copyState;
    switch (action.type) {
        case START_LOADING_TICTAC:
            copyState = {...state};
            copyState.isLoading = true;
            return copyState;
        case LOADED_TICTACS:
            copyState = {...state};
            copyState.isLoading = false;
            copyState.tictacs = [...action.payload];
            return copyState;
        case ADD_TICTAC :
            copyState = {...state};
            copyState.tictacs = [...copyState.tictacs, action.payload];
            return copyState;
        default:
            return state;
    }
};

export default chatReducer;