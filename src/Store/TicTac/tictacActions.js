import axios from 'axios';
import {ADD_TICTAC, LOADED_TICTACS, START_LOADING_TICTAC} from "./tictacTypes";


const tokenConfig = (getState) => {
    // Get token from localstorage
    const token = getState().auth.token;

    // Headers
    const config = {
        headers: {
            'Content-type': 'application/json'
        }
    };

    // If token, add to headers
    if (token) {
        config.headers['todo-auth-token'] = token;
    }

    return config;
};

export const loadTictacs = () => {
    return (dispatch, getState)=>{
        dispatch(startLoadingTicTacsAC());
        axios.get('/tictac', tokenConfig(getState)).then(res=>{
            dispatch(loadedTicTac(res.data));
        }).catch(error=>{console.log(error)});
    }
};

export const createTictac = (name) =>{
    return (dispatch, getState)=>{
        axios.post('/tictac/createroom', {name}, tokenConfig(getState)).then(res=>{
            dispatch(addTicTacAC(res.data));
        }).catch(error =>{
            console.log(error);
        })
    }
};

export const startLoadingTicTacsAC = () =>({
    type: START_LOADING_TICTAC
});

export const loadedTicTac = (payload) =>({
    type: LOADED_TICTACS, payload: payload
});

export const addTicTacAC = (payload) => ({
    type: ADD_TICTAC, payload: payload
});
