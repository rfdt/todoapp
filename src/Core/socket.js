import io from 'socket.io-client';

const socket = io();

socket.on('disconnect', () => {
    socket.open();
});

export default socket;